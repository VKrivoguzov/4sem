#include <iostream>
#include <conio.h>

float getMinDenorm() {
	float minDenorm = 1;
	float b = 1;
	while (b != 0)
	{
		minDenorm = b;
		b /= 2;
	}
	return minDenorm;
}

float getMinNorm() {	
	float minDenorm = getMinDenorm();
	float b=minDenorm, a=2*minDenorm;
	while (a-b ==minDenorm) {
		b = a;
		a += minDenorm;
	}	
	return b/2;
}

int main() {
	setlocale(0, "");
	std::cout.precision(100);
	float two = 2;
	
	std::cout << "����������� ����������������� " << getMinDenorm() << std::endl;
	std::cout << "��� ������ " << FLT_TRUE_MIN<<std::endl;
	
	std::cout << "����������� ��������������� " << getMinNorm() << std::endl;
	std::cout << "� ��� ���� ������ " << FLT_MIN << std::endl;
	_getch();
	return 0;
}