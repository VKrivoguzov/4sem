#pragma once
#include <process.h>
#include <windows.h>  

struct metadata {
	int* startPointer;
	int size;
	metadata* prev;
	metadata* next;
	bool isFree;
};

class SyncHeap
{
private:
	int* heap;
	int maxHeapSize; 
	metadata* blockList;
	CRITICAL_SECTION alocationCS; 
	CRITICAL_SECTION releaseCS;
	HANDLE hMemoryReleasedEvent;
	HANDLE hFullQueueEvent;
	HANDLE barierSemafor;
public:
	SyncHeap(int nSize); // nSize - ������ ����
	~SyncHeap();
	int* allocate(int nElement); // �������� �������� �� ����
	void free(int* nPtr); // ������� �������� � ����
	void print(); // �������� ������, ��� �������
};

