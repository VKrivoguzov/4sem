#include "SyncHeap.h"
#include <iostream>
#include <algorithm>
#include <vector>
#include <process.h>
#include <windows.h>  
#include <conio.h>  
//������� 13:00 508 14:30 604
//������� 14:30 608
SyncHeap::SyncHeap(int n)
{
	maxHeapSize = n;
	heap = new int[n];
	blockList = new metadata;
	blockList->startPointer = heap;
	blockList->isFree = true;
	blockList->size = n;
	blockList->next = NULL;
	blockList->prev = NULL;
	barierSemafor = CreateSemaphore(NULL, 0, INFINITE, NULL);
	hMemoryReleasedEvent = CreateEvent(NULL, true, false, NULL);
	hFullQueueEvent = CreateEvent(NULL, true, true, NULL);
	InitializeCriticalSection(&alocationCS);
	InitializeCriticalSection(&releaseCS);
}

SyncHeap::~SyncHeap()
{
	delete heap;
	while (blockList != NULL) {
		metadata* temp = blockList;
		blockList = blockList->next;
		delete temp;
	}
	DeleteCriticalSection(&alocationCS);
	DeleteCriticalSection(&releaseCS);
}
int currentInSemafor = 0;
int queueNumber = 0;
int waitedEvent = 0;

int* SyncHeap::allocate(int nElement)
{
	if (nElement <= 0)
		return NULL;
	metadata* current;

	queueNumber++;
	do {
		EnterCriticalSection(&alocationCS);		
		//����� ����������� �����
		current = blockList;
		while (current != NULL && (!current->isFree || current->size < nElement))
			current = current->next;
		//�����
		if (current != NULL)
			break;
		//�� �����
		ResetEvent(hFullQueueEvent);
		currentInSemafor++;
		if (currentInSemafor == queueNumber) //���� ��� ������ �������� ����, ������ "������"
			ReleaseSemaphore(barierSemafor, currentInSemafor,NULL);

		LeaveCriticalSection(&alocationCS);

		WaitForSingleObject(barierSemafor, INFINITE);//"������" ���� ���� ��� �� ������
		currentInSemafor--;
		ResetEvent(hMemoryReleasedEvent);
		waitedEvent++;			
		if (waitedEvent == queueNumber)
			SetEvent(hFullQueueEvent);
		HANDLE handles[] = { hMemoryReleasedEvent ,hFullQueueEvent };
		WaitForMultipleObjects(2, handles, true, INFINITE);
	} while (true);
	queueNumber--;

	//������� ����
	if (current->size != nElement) {
		metadata* newBlock = new metadata;
		newBlock->isFree = true;
		Sleep(7);
		newBlock->next = current->next;
		Sleep(7);
		newBlock->prev = current;
		Sleep(7);
		newBlock->startPointer = current->startPointer + nElement;
		Sleep(7);
		newBlock->size = current->size - nElement;
		Sleep(7);
		current->next = newBlock;
	}
	current->isFree = false;
	current->size = nElement;
	LeaveCriticalSection(&alocationCS);
	return current->startPointer;
}
void SyncHeap::free(int* nPtr)
{
	//���
	metadata* current = blockList;
	EnterCriticalSection(&releaseCS);
	while (current != NULL && (current->startPointer != nPtr || current->isFree))
		current = current->next;
	if (current == NULL) {
		LeaveCriticalSection(&releaseCS);
		return;
	}
	current->isFree = true;

	if (current->next != NULL && current->next->isFree) {
		metadata* temp = current->next;
		current->size += temp->size;
		current->next = temp->next;
		if (temp->next != NULL)
			temp->next->prev = current;

		delete temp;
	}
	if (current->prev != NULL && current->prev->isFree) {
		metadata* temp = current->prev;
		temp->size += current->size;
		temp->next = current->next;
		if (current->next != NULL)
			current->next->prev = temp;

		delete current;
	}
	LeaveCriticalSection(&releaseCS);
	WaitForSingleObject(hFullQueueEvent, INFINITE);
	waitedEvent = 0;
	SetEvent(hMemoryReleasedEvent);
}

void SyncHeap::print()
{
	std::cout << "--------------";
	for (int i = 0; i < maxHeapSize; i++)
		std::cout << heap[i] << " ";
	std::cout << "--------------\n";
}