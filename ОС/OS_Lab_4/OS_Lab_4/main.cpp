#include <iostream>
#include <algorithm>
#include <vector>
#include <process.h>
#include <windows.h>  
#include <conio.h>  
#include "SyncHeap.h"

using namespace std;

extern void userFunc(int i);
SyncHeap* heap;
CRITICAL_SECTION cs;
int main() 
{
	InitializeCriticalSection(&cs);
	setlocale(0, "");
	int heapSize;
	int nUser;
	// ����
	cout << "������� ������ ����" << endl;
	cin >> heapSize;
	heap = new SyncHeap(heapSize);

	cout << "������� ���������� ������" << endl;
	cin >> nUser;
	HANDLE* hEndEvent = new HANDLE[nUser];
	HANDLE* hUserThreads = new HANDLE[nUser];

	cout << "1 - ������������� ������" << endl;
	cout << "2 - ������������ ������" << endl;
	cout << "3 - ����������" << endl;
	cout << "4 - ���������" << endl;
	for (int i = 0; i < nUser; i++)
	{
		int index = i;
		hEndEvent[i] = CreateEvent(NULL, FALSE, FALSE, NULL);
		hUserThreads[i] = (HANDLE)_beginthreadex(NULL, 0, (_beginthreadex_proc_type)(userFunc), (void*)(index+1), 0, NULL);
	}
	WaitForMultipleObjects(nUser, hUserThreads, TRUE, INFINITE);
	DeleteCriticalSection(&cs);
	return 0;
}