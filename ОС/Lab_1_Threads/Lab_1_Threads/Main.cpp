#include <iostream>
#include <process.h>
#include <windows.h>  
#include <mutex.h>

double** matrix;
double* vector;
double* resultVector;	
int n;
std::mutex coutMutex;

void mult(void* i) {	
	for (int j = 0; j < n; j++)
	{
		resultVector[(int)i] += matrix[(int)i][j] * vector[j];
	}
	coutMutex.lock();
	std::cout << "������������ ������ " << (int)i+1 << " �� ������ ����� " << resultVector[(int)i]<<std::endl;
	coutMutex.unlock();
}

int main() {
	setlocale(0,"");
	std::cout <<"������� ����������� �������"<<std::endl;
	std::cin >> n;
	
	vector = new double[n];
	resultVector = new double[n];

	matrix = new double*[n];
	for (int i = 0; i < n; i++) {
		matrix[i] = new double[n];
	}

	std::cout << "������� �������� �������" << std::endl;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++)
		{
			std::cin >> matrix[i][j];
		}
	}
	std::cout << "������� �������� �������"<< std::endl;
	for (int i = 0; i < n; i++)
	{
		std::cin >> vector[i];
	}



	UINT* IDThread= new UINT[n];
	HANDLE* hThreads = new HANDLE[n];
	for (size_t i = 0; i < n; i++)
	{
		int index = i;
		hThreads[i] = (HANDLE)_beginthreadex(NULL, 0, (_beginthreadex_proc_type)(mult), (void*)i, 0, &IDThread[i]);
		Sleep(7);
	}	
	WaitForMultipleObjects(n, hThreads, true, INFINITE);

	std::cout << "�������������� ������ ";
	for  (int i = 0; i < n;i++)
	{
		std::cout << resultVector[i] << " ";
	}


	for (int i = 0; i < n; i++) {
		delete[] (matrix[i]);
	}
	delete[] matrix;
	delete[] vector;
	delete[] resultVector;
	return 0;
}