#include <iostream>
#include <fstream>
#include <windows.h>
#include "Student.h"
using namespace std;

HANDLE pipeCreate() {
	SECURITY_ATTRIBUTES sa; // �������� ������
	SECURITY_DESCRIPTOR sd; // ���������� ������
	HANDLE hNamedPipe;
	// ������������� ��������� ������
	sa.nLength = sizeof(sa);
	sa.bInheritHandle = FALSE; // ���������� ������ �������������
							   // �������������� ���������� ������
	InitializeSecurityDescriptor(&sd, SECURITY_DESCRIPTOR_REVISION);
	// ������������� �������� ������, �������� ������ ���� �������������
	SetSecurityDescriptorDacl(&sd, TRUE, NULL, FALSE);
	sa.lpSecurityDescriptor = &sd;



	hNamedPipe = CreateNamedPipe("\\\\.\\pipe\\my_pipe", PIPE_ACCESS_DUPLEX, PIPE_TYPE_MESSAGE | PIPE_WAIT, 1, 0, 0, INFINITE, &sa);
	if (hNamedPipe == INVALID_HANDLE_VALUE)
	{
		cerr << "Creation of the named pipe failed." << endl
			<< "The last error code: " << GetLastError() << endl;
		cout << "Press any char to finish server: ";
	}	return hNamedPipe;
}

void waitForClient(HANDLE hNamedPipe) {
	if (!ConnectNamedPipe(hNamedPipe,(LPOVERLAPPED)NULL))
	{
		cerr << "The connection failed." << endl
			<< "The last error code: " << GetLastError() << endl;
		CloseHandle(hNamedPipe);
		cout << "Press any char to finish the server: ";
	}
}


int main() {
	setlocale(0, "");
	int n = 0;

	fstream out("Students", ios::binary | ios::in | ios::out);
	cout << "������� ���������� �������\n";
	cin >> n;
	student* studentArr = new student[n];
	cout << "����� - ��� - ����\n";
	for (int i = 0; i < n; i++) {
		cin >> studentArr[i].num >> studentArr[i].name >> studentArr[i].grade;
		out.seekg(i * sizeof(student), ios_base::beg);
		out.write((char*)&studentArr[i], sizeof(student));
	}
	
	HANDLE hNamedPipe = pipeCreate();
	cout << "��� ����������� �������" << endl;
	char buf[80];
	DWORD dwBytesRead;
	DWORD dwBytesWrite;	waitForClient(hNamedPipe);	cout << "������ ���������\n";	int index = 0;	student st;	st.num = 0;	while (ReadFile(hNamedPipe, buf, sizeof(buf), &dwBytesRead, (LPOVERLAPPED)NULL)) {		if (dwBytesRead == 4) {			int key = (int)buf[0];			cout << "���� " << key << endl;			if (st.num != key)				for (index = 0; st.num != key && index < n; index++) {					out.seekg((int)index * sizeof(student), ios_base::beg);					out.read((char*)&st, sizeof(student));				}			cout << st.num << " " << st.name <<" "<< st.grade << " ������ �������\n";			WriteFile(hNamedPipe, &st, sizeof(student), &dwBytesWrite, NULL);		}		else {			index--;			cout << index << endl;			out.seekg(index * sizeof(student), ios_base::beg);
			cout << "������� �� " << ((student*)buf)->name << " " << ((student*)buf)->grade;
			out.write((char*)((student*)buf), sizeof(student));			st.num = -1;		}	}
	DisconnectNamedPipe(hNamedPipe);
	return 0;
}