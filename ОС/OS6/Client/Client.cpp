#include <iostream>
#include <fstream>
#include <windows.h>
#include "../OS6/Student.h"
#include "conio.h"
#include <string>
using namespace std;
HANDLE connect() {
	cout << "Enter a name of the server machine: ";
	
	char machineName[80];
	char pipeName[80];
	cin >> machineName;
	wsprintf(pipeName, "\\\\%s\\pipe\\my_pipe",
			machineName);
	HANDLE hNamedPipe = CreateFile(pipeName,GENERIC_READ | GENERIC_WRITE,		FILE_SHARE_READ | FILE_SHARE_WRITE, (LPSECURITY_ATTRIBUTES)NULL,OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL,(HANDLE)NULL );
	// ��������� ����� � �������
	if (hNamedPipe == INVALID_HANDLE_VALUE)
	{
		cerr << "Connection with the named pipe failed." << endl
			<< "The last error code: " << GetLastError() << endl;		
		return NULL;
	}
	return hNamedPipe;
}

int main() {
	setlocale(0, "");
	HANDLE hNamedPipe= connect();
	cout << "1 - ����������� ������ �����\n";
	cout << "2 - ������ ������\n";
	cout << "3 - ����� �� �����\n";
	char option = 0;
	int key=0;

	student st;
	DWORD bytesNumber = 0;
	while (option != '3') {  // ����� �� �����;

		option = _getch();
		switch (option)
		{
		case '1':  // ����������� ������ �����;
			cout << "������� ���� ������\n";
			cin >> key;
			WriteFile(hNamedPipe, &key, sizeof(key), &bytesNumber, NULL);
			ReadFile(hNamedPipe, &st, sizeof(student), &bytesNumber, NULL);
			cout << st.num << " " << st.name << " " << st.grade<<endl;
			cout << "��������\n";
			cin >> st.name >> st.grade;
			WriteFile(hNamedPipe, &st, sizeof(student), &bytesNumber, NULL);
			break;
		case '2': // ������ ������;
			cout << "������� ���� ������\n";
			cin >> key;
			WriteFile(hNamedPipe, &key, sizeof(key), &bytesNumber, NULL);			
			ReadFile(hNamedPipe, &st, sizeof(student), &bytesNumber, NULL);
			cout << st.num <<" " <<st.name << " " << st.grade << endl;
			break;
		}
	}
	CloseHandle(hNamedPipe);
	_getch;
	return 0;
}