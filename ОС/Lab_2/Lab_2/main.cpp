#include <windows.h>
#include <conio.h>
#include <iostream>
#include <string>
#include "Header.h"
using namespace std;

HANDLE hWritePipe*;
HANDLE hReadPipe*;
HANDLE hInheritWritePipe*;
HANDLE hEnableRead*;


bool runProcces(string str) {
	char lpszAppName[MAX_PATH + 10];
	strcpy_s(lpszAppName, str.c_str());
	STARTUPINFO si;
	PROCESS_INFORMATION piApp;
	ZeroMemory(&si, sizeof(STARTUPINFO));
	si.cb = sizeof(STARTUPINFO);
	if (!CreateProcess(NULL, lpszAppName, NULL, NULL, FALSE,
		CREATE_NEW_CONSOLE, NULL, NULL, &si, &piApp))
		return false;
	WaitForSingleObject(piApp.hProcess, INFINITE);
	CloseHandle(piApp.hThread);
	CloseHandle(piApp.hProcess);
}

bool createPipe(int i) {
	STARTUPINFO si;
	SECURITY_ATTRIBUTES sa;
	PROCESS_INFORMATION pi;
	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor = NULL; // ������ �� ���������
	sa.bInheritHandle = TRUE;
	return CreatePipe(&hReadPipe[i], &hWritePipe[i], &sa, 0));
}

int main(int argc, char *argv[])
{
	setlocale(0, "");

	int n;
	cout << "������� ���������� ��������\n";
	cin >> n ;

	hWritePipe = new HANDLE[n];
	hReadPipe = new HANDLE[n];
	hInheritWritePipe = new HANDLE[n];
	hEnableRead = new HANDLE[n]; 

	for (int i = 0; i < n; i++) {
		string eventName = "Enable read " + i;
		hEnableRead = CreateEvent(NULL, FALSE, FALSE, eventName.c_str());
		hInheritWritePipe[i] = createPipe(i);
		runProcces("Client.exe " + i + hInheritWritePipe[i]);
	}

	while (true) {//�������������
		message mes;
		ReadFile(hReadPipe[0], &mes, sizeof(mes), &dwBytesRead, NULL));//����������, 0
		DWORD dwBytesWritten;
		WriteFile(hWritePipe[0], mes.data.c_str(), sizeof(mes.data.c_str()), &dwBytesWritten, NULL)); //���� ������������
	}

	for (int i = 0; i < n; i++) {
		CloseHandle(hWritePipe[i]);
		CloseHandle(hReadPipe[i]);
		CloseHandle(hInheritWritePipe[i]);
		CloseHandle(hEnableRead[i]);
	}
	_getch();
	return 0;
}