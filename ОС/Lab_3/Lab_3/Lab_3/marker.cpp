#include <iostream>
#include <process.h>
#include <windows.h>  

using namespace std;

extern HANDLE hMarkerRunEvents;
extern HANDLE* hMarkerReady;
extern HANDLE* hEndMarker;
extern int n;
extern int* arr;
extern CRITICAL_SECTION cs;

void marker(int i) {
	SetEvent(hMarkerReady[i]);
	WaitForSingleObject(hMarkerRunEvents, INFINITE);
	srand(i+1);
	while (true)
	{
		int index = rand() % n;

		EnterCriticalSection(&cs);
		if (arr[index] == 0) {
			Sleep(5);
			arr[index] = i + 1;
			LeaveCriticalSection(&cs);
			Sleep(5);
		}
		else {
			cout << "����� �" << i + 1;
			int count = 0;
			for (int j = 0; j < n; j++) {
				if (arr[j] == i+1)
					count++;
			}
			cout << ", ���������� " << count << ", ���������� �������� ������� � �������� " << index + 1 << endl;
			LeaveCriticalSection(&cs);
			HANDLE h[2] = { hEndMarker[i],hMarkerRunEvents };
			SetEvent(hMarkerReady[i]);
			if (WaitForMultipleObjects(2, h, false, INFINITE) == WAIT_OBJECT_0) {
				for (int j = 0; j < n; j++) {
					if (arr[j] == i + 1)
						arr[j] = 0;
				}
				break;
			}
		}
	
	}
}

