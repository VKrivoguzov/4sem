#include <iostream>
#include <process.h>
#include <windows.h>  
#include <conio.h>  

using namespace std;

extern void marker(int i);
HANDLE hMarkerRunEvents;
HANDLE* hMarkerReady;
HANDLE* hEndMarker;
int n;
int* arr;
CRITICAL_SECTION cs;

int main() {
	setlocale(0, "");
	int threadNumber;
	hMarkerRunEvents = CreateEvent(NULL, TRUE, FALSE, NULL);
	InitializeCriticalSection(&cs);
	cout << "������� ������ �������\n";
	cin >> n;
	
	cout << "������� ����� �������\n";
	cin >> threadNumber;
	
	arr = new int[n];
	fill(arr, arr + n, 0);

	UINT* IDThread = new UINT[threadNumber];
	HANDLE* hThreads = new HANDLE[threadNumber];
	
	hMarkerReady = new HANDLE[threadNumber];
	hEndMarker = new HANDLE[threadNumber];

	for (int i = 0; i < threadNumber; i++)
	{
		int index = i;
		hMarkerReady[i] = CreateEvent(NULL, TRUE, FALSE, NULL);
		hEndMarker[i] = CreateEvent(NULL, TRUE, FALSE, NULL);
		hThreads[i] = (HANDLE)_beginthreadex(NULL, 0, (_beginthreadex_proc_type)(marker), (void*)index, 0, &IDThread[i]);
	}

	WaitForMultipleObjects(threadNumber, hMarkerReady, true, INFINITE);	
	for (int i = 0; i < threadNumber; i++) {		
			ResetEvent(hMarkerReady[i]);
	}
	PulseEvent(hMarkerRunEvents);
	
	for (int i = 0; i < threadNumber; i++) {		
		WaitForMultipleObjects(threadNumber, hMarkerReady, true, INFINITE);		
		for (int i = 0; i < n; i++) 
			cout << arr[i] << " ";
		cout << endl;	
		int number = 1;
		cout << "������� ����� ������\n";
		cin >> number;
		number--;
		SetEvent(hEndMarker[number]);
		WaitForSingleObject(hThreads[number], INFINITE);
		CloseHandle(hThreads[number]);
		for (int i = 0; i < threadNumber; i++) {
			if (WaitForSingleObject(hEndMarker[i],0)!=WAIT_OBJECT_0)
				ResetEvent(hMarkerReady[i]);
		}
		PulseEvent(hMarkerRunEvents);
	}
	DeleteCriticalSection(&cs);

	return 0;
}