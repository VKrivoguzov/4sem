package Entity;


import javafx.beans.property.*;
import sample.TypeOfProduct;

/**
 * @author Krivoguzov
 * @version 1.0
 * @created 24-���-2018 13:47:27
 */
public class Product {
	public IntegerProperty ID;
	public StringProperty name;
	public DoubleProperty price;
	public StringProperty article;

	public Product(int ID,String name, double price, String article){
		this.ID = new SimpleIntegerProperty(ID);
		this.name = new SimpleStringProperty(name);
		if(price<0)
			throw new IllegalArgumentException();
		this.price = new SimpleDoubleProperty(price);
		this.article = new SimpleStringProperty(article);
	}
	public Product(){
        ID = new SimpleIntegerProperty(0);
		name = new SimpleStringProperty("Имя товара");
		price = new SimpleDoubleProperty(100);
		article = new SimpleStringProperty("Артикул");
	}

	public String getName(){
		return name.get();
	}
	public int getID(){
		return ID.get();
	}
	public double getPrice(){
		return price.get();
	}
	public String getArticle(){
		return article.get();
	}
	public void finalize() throws Throwable {

	}

	public String toString(){
		return getName()+getPrice()+getArticle();
	}

}