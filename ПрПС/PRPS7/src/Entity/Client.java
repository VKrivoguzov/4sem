package Entity;

import java.sql.Date;
import java.time.LocalDate;

import javafx.beans.property.*;


public class Client {
	public double discount;
	public StringProperty firstName;
	public StringProperty lastName;
	public StringProperty patronymic;
	public StringProperty phoneNumber;

	public IntegerProperty sale;
	private final ObjectProperty<LocalDate> birthday;
	private int ID;
	public Client(String name,String lastName, String patronymic, String phoneNumber, LocalDate birthday){
		firstName = new SimpleStringProperty(name);
		this.lastName = new SimpleStringProperty(lastName);
		this.patronymic = new SimpleStringProperty(patronymic);
		this.phoneNumber = new SimpleStringProperty(phoneNumber);
		this.birthday = new SimpleObjectProperty<LocalDate>(birthday);
		sale = new SimpleIntegerProperty(0);

	}
	public void setSale(int s){
		sale.setValue(s);
	}
	public StringProperty getName(){
		return firstName;
	}

	public StringProperty getLastName(){
			return lastName;
	}

	public StringProperty getPatronymic(){
		return patronymic;
	}

	public StringProperty getPhoneNumber(){
		return phoneNumber;
	}
	public void setID(int id){
		this.ID = id;
	}
	public int getID(){
		return ID;
	}
	public StringProperty getBirthday(){
		return new SimpleStringProperty(DateFormat.format(birthday.get()));
	}
	@Override
    public String toString(){
	    return getName()+" "+getLastName()+" "+ getPatronymic()+" "+ getPhoneNumber()+" "+(getBirthday().get());
    }
}