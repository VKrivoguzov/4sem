package Entity;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;

import java.sql.*;
import java.sql.Driver;
import java.time.LocalDate;
import java.util.ArrayList;
import javafx.collections.FXCollections;

import javafx.collections.ObservableList;
import Entity.DatabaseManager;
import Entity.Product;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import Entity.*;
import java.io.IOException;

/**
 * @author Krivoguzov
 * @version 1.0
 * @created 24-���-2018 13:47:18
 */
public class DatabaseManager {

	//private Database entity db;
    private static final String url = "jdbc:mysql://localhost:3306/sales";
	private static final String user = "root";
	private static final String password = "root";

	// JDBC variables for opening and managing connection
	private static Connection con;
	private static Statement stmt;
	private static ResultSet rs;


	public DatabaseManager(){

		try {
			con = DriverManager.getConnection(url, user, password);
			System.out.println(con.toString());
			stmt = con.createStatement();
		}catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		}
	}

	public void close(){
        try { con.close(); } catch(SQLException se) { /*can't do anything */ }
        try { stmt.close(); } catch(SQLException se) { /*can't do anything */ }
        try { rs.close(); } catch(SQLException se) { /*can't do anything */ }
    }

	public void addProducts(){

	}

	public void addClient(Client client){
		String query = String.format("insert into sales.clients(Name,Lastname,Patronymic,Phone_number,Birthday) values(\'%s\',\'%s\',\'%s\',\'%s\',\'%s\')",
				client.getName().get().toString(),client.getLastName().get().toString(),client.getPatronymic().get().toString(),client.getPhoneNumber().get().toString(),client.getBirthday().get().toString());

		try {
			stmt.executeUpdate(query);
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		}
	}

	public Order getOrder(int id){
	    System.out.println(id);
        System.out.println(id);
        System.out.println(id);
        String query = "select * from sales.order where Order_ID="+id;
        Order order=null;
        try {
            rs = stmt.executeQuery(query);
            rs.next();
            int ID = rs.getInt(1);
            int Client_ID = rs.getInt(2);
            LocalDate delivery =null;
            if( rs.getDate(3)!=null)
                delivery = rs.getDate(3).toLocalDate();
            LocalDate creation = rs.getDate(4).toLocalDate();
            order = new Order(ID,Client_ID,delivery,creation);
            query = "select * from sales.order_item where Order_ID="+id;
            rs = stmt.executeQuery(query);
            while(rs.next()){
                Product product =new Product();
                product.ID=new SimpleIntegerProperty(rs.getInt(2));
                order.addProduct(product,rs.getInt(3));
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
        return order;
	}

	public void createUser(){

	}
	public void addOrder(Order order){
		String query1=null;
		order.creationDate = new SimpleObjectProperty<LocalDate>(LocalDate.now());
		if(order.deliveryDate != null)
			query1 = String.format("insert into sales.order(Client_ID,Delivery_date,Creation_date) values(\'%s\',\'%s\',\'%s\')",
				Integer.toString(order.client.getID()),order.deliveryDate.get().toString(), order.creationDate.get().toString());
		else
			query1 = String.format("insert into sales.order(Client_ID,Creation_date) values(\'%s\',\'%s\')",
					Integer.toString(order.client.getID()), order.creationDate.get().toString());

		try {
			stmt.executeUpdate(query1);
			String query = String.format("select * from sales.order where Client_ID=\'%s\'",order.client.getID());//,order.creationDate.get().toString());//, Creation_date='%s'
			rs = stmt.executeQuery(query);
			rs.next();
			int orderID = rs.getInt(1);
			PreparedStatement statement = con.prepareStatement("insert into sales.order_item(Order_ID, Product_ID, Amount)  values(?,?,?)");
			statement.setInt(1,orderID);
			for (ProductPosition i: order.productList ) {
				statement.setInt(2,i.product.ID.get());
				statement.setInt(3,i.number.get());
				statement.addBatch();
			}
			statement.executeBatch();
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		}

	}

	public ObservableList<Client> getBirthdayClient(LocalDate bd,Integer sale){
        ObservableList<Client> clients = FXCollections.observableArrayList();
        Client client=null;
        try {
            String query="select * from sales.clients";
            if(sale!=null)
                query = "select * from sales.clients where sale="+sale;
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                LocalDate birthday = rs.getDate(6).toLocalDate();
                if(bd!=null) {
                    if (birthday.getDayOfYear() < bd.getDayOfYear() || birthday.getDayOfYear() > bd.getDayOfYear() + 10)
                        continue;
                }
                String name = rs.getNString(2);
                String lastName = rs.getNString(3);
                String patronymic = rs.getNString(4);
                String phoneNumber = rs.getNString(5);
                client = new Client(name,lastName, patronymic,phoneNumber,birthday);
                System.out.println(client);
                clients.add(client);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
		return clients;
	}
	public void deleteOrder(){

	}
	public Product getProduct(int product_ID){
		String query = "select * from sales.products where Product_ID="+product_ID;
		Product product=null;
		try {
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				int ID = rs.getInt(1);
				String name = rs.getNString(2);
				Double price = rs.getDouble(3);
				String description = rs.getNString(4);
				product = new Product(ID,name,price,description);
				System.out.println(product);
			}
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		}
		return product;
	}
    public Client getClient(int client_ID){
        String query = "select * from sales.clients where Client_ID="+client_ID;
        Client client=null;
        try {
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                String name = rs.getNString(2);
                String lastName = rs.getNString(3);
                String patronymic = rs.getNString(4);
                String phoneNumber = rs.getNString(5);
                LocalDate birthday = rs.getDate(6).toLocalDate();
                client = new Client(name,lastName, patronymic,phoneNumber,birthday);
            	client.setID(rs.getInt(1));
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
        return client;
    }
	public Client getClient() {
		Client client=null;
		try {
			String query = "select * from sales.clients";
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				String name = rs.getNString(2);
				String lastName = rs.getNString(3);
				String patronymic = rs.getNString(4);
				String phoneNumber = rs.getNString(5);
				LocalDate birthday = rs.getDate(6).toLocalDate();
				client = new Client(name,lastName, patronymic,phoneNumber,birthday);
				System.out.println(client);
			}
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		}
		return client;
	}

	public void updateOrder(){

	}

}