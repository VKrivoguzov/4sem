package Entity;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class ProductPosition {
    public Product product;
    public IntegerProperty number;
    public ProductPosition(Product product, int number){
        if(number>0 && product!=null) {
            this.number = new SimpleIntegerProperty(number);
            this.product = product;
        }
    };


}
