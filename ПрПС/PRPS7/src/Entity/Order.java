package Entity;


import java.time.LocalDate;
import java.util.ArrayList;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

public class Order {

	//private Date creationDate;
	//private Date deliveryDate;
	public IntegerProperty ID;
	public ArrayList<ProductPosition> productList;
	public Client client;
    public int clientID;
    public ObjectProperty<LocalDate> deliveryDate;
    public ObjectProperty<LocalDate> creationDate;

    public Order(){
        productList = new ArrayList<ProductPosition>();
        deliveryDate = null;
        creationDate = null;
    }
	public Order(int ID,Client client ){
	    productList = new ArrayList<ProductPosition>();
		this.ID = new SimpleIntegerProperty(ID);
		this.client = client;
        deliveryDate = null;
        creationDate = null;
	}
    public Order(int ID,int client_id,LocalDate l1,LocalDate l2 ){
        productList = new ArrayList<ProductPosition>();
        this.ID = new SimpleIntegerProperty(ID);
        this.client = null;
        clientID =client_id;
        if(!l1.isAfter(l2))
            throw new IllegalArgumentException();
        deliveryDate = new SimpleObjectProperty<>(l1);
        creationDate  = new SimpleObjectProperty<>(l2);
    }
    public void addProduct(Product product, int number){
        for (int i=0;i<productList.size();i++) {
            ProductPosition pos = productList.get(i);
            if (pos.product.getID() == product.getID()) {
                pos.number.setValue(pos.number.get()+number);
                return;
            }
        }
        productList.add(new ProductPosition(product,number));
    }
    public void deleteProduct(Product product){
        for (int i=0;i<productList.size();i++) {
            ProductPosition pos = productList.get(i);
            if (pos.product.getID() == product.getID()) {
                productList.remove(i);
                return;
            }
        }
    }
    double getPrice(){
	    double price = 0;
        for (ProductPosition pos: productList) {
            price+=pos.product.getPrice();
        }
        return price;
    }
	public void finalize() throws Throwable {

	}

	public boolean addItem(){
		return false;
	}

	public String toString(){
		return "";
	}

}