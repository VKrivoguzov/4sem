package Entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
public class Tests {
    @Test
    public void clientCreating() {
        Client c = new Client("Кривогузов","Владислав","Валерьевич","+375292992894",DateFormat.parse("11.11.1111"));
        assertNotNull(c);
    }
    @Test
    public void datePositiveParse() {
          assertNotNull(DateFormat.parse("19.03.1998"));
    }
    @Test
    public void dateNegativeParse() {
        assertEquals(DateFormat.parse("19.03-1998"),null);
    }

    @Test
    public void datePositiveOrder() {
        Order order = new Order(1,1,DateFormat.parse("12.11.2019"),DateFormat.parse("11.11.2017"));
        assertNotNull(order);
    }
    @Test
    public void dateNegativeOrder() {
        try {
            Order order = new Order(1, 1, DateFormat.parse("12.11.2019"), DateFormat.parse("11.11.2020"));
        }catch(Exception e){
            assertNotNull(e);
        }
    }
    @Test
    public void negativePrice() {
        try {
            Product product = new Product(1, "1", -1, "");
        }catch(Exception e){
            assertNotNull(e);
        }
    }
    @Test
    public void positiveProduct() {
        Product product = new Product(1, "1", 1, "");
        assertNotNull(product);
    }
}