package sample;


/**
 * @author Krivoguzov
 * @version 1.0
 * @created 24-���-2018 13:47:56
 */
public enum TypeOfProduct {
	clothing,
	products,
	footwear,
	leather_goods,
	hosiery
}