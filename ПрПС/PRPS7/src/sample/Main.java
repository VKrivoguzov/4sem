package sample;

import Entity.DatabaseManager;
import Entity.Product;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import Entity.*;
import java.io.IOException;

public class Main extends Application {


    private Stage primaryStage;

    private BorderPane rootLayout;
    static private ObservableList<ProductPosition> productList = FXCollections.observableArrayList();
    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("sample.fxml"));
        Parent rootLayout = loader.load();
        primaryStage.setTitle("Sales analysis system");
        primaryStage.setScene(new Scene(rootLayout, 780, 540));
        primaryStage.show();

       ProductController controller = loader.getController();
       controller.setMainApp(this);
    }

    public static DatabaseManager db;
    public static void main(String[] args) {
        //////////////
        db = new DatabaseManager();

        System.out.print(new Product().ID);
        launch(args);


    }
    public void closeDB(){
        db.close();
    }
    public ObservableList<ProductPosition> getPersonData() {

        System.out.print("getPersonal");
        return productList;

    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public void showPersonEditDialog(ProductPosition product) {
        try {
            // Загружаем fxml-файл и создаём новую сцену
            // для всплывающего диалогового окна.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("PruductInfoDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Создаём диалоговое окно Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Информация о продукте");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Передаём адресата в контроллер.
            ProductInfoDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setProduct(product);

            // Отображаем диалоговое окно и ждём, пока пользователь его не закроет
            dialogStage.showAndWait();;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
