package sample;

import Entity.Client;
import Entity.DatabaseManager;
import Entity.DateFormat;
import Entity.*;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.collections.ObservableList;
import javafx.stage.Stage;

import javafx.scene.control.TextField;

import java.time.LocalDate;

public class ProductController {

    @FXML
    private TableView<ProductPosition> personTable;
    @FXML
    private TableColumn<ProductPosition, Integer> id;
    @FXML
    private TableColumn<ProductPosition, String> nameColumn;
    @FXML
    private TableColumn<ProductPosition, Double> priceColumn;
    @FXML
    private TableColumn<ProductPosition, String> articleColumn;
    @FXML
    private TableColumn<ProductPosition, Integer> amountColumn;
    @FXML private TableColumn<ProductPosition, String> amountLabel;
    @FXML private TextField product_ID;

    private Main mainApp;

    public ProductController() {
    }

    @FXML
    private void initialize() {
        CB.getItems().addAll(
                "Ближайшие дни рождения",
                "Скидка",
                "Все"
        );
        currentOrder = new Order();
        // Инициализация таблицы адресатов с 5 столбцами.
        CB.getSelectionModel().select(2);
        id.setCellValueFactory(cellData -> cellData.getValue().product.ID.asObject());
        nameColumn.setCellValueFactory(cellData -> cellData.getValue().product.name);
        articleColumn.setCellValueFactory(cellData -> cellData.getValue().product.article);
        priceColumn.setCellValueFactory(cellData -> cellData.getValue().product.price.asObject());
        amountColumn.setCellValueFactory(cellData -> cellData.getValue().number.asObject());
        //Для доп инфы
        personTable.setRowFactory( tv -> {
            TableRow<ProductPosition> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    ProductPosition rowData = row.getItem();
                    mainApp.showPersonEditDialog(rowData);
                }
            });
            return row ;
        });
        //Client_id textfield is changed
        clientIDTB.focusedProperty().addListener((obs, oldPropertyValue, newPropertyValue) -> {
            if(oldPropertyValue){
                int id = Integer.parseInt(clientIDTB.getText());
                currentOrder.client = mainApp.db.getClient(id);
                System.out.print(currentOrder.client.getName());
            }
        });
    }

    public void setMainApp(Main mainApp) {
        this.mainApp = mainApp;
        // Добавление в таблицу данных из наблюдаемого списка
        personTable.setItems(mainApp.getPersonData());
    }
    //@FXML private Button deleteButton;
    @FXML private void handleDeletePosition() {
        int selectedIndex = personTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            currentOrder.deleteProduct(personTable.getItems().get(selectedIndex).product);
            personTable.getItems().remove(selectedIndex);
        }
        else{
            // Ничего не выбрано.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Product Position Selected");
            alert.setContentText("Please select a Product Position in the table.");
            alert.showAndWait();
        }

    }
    //TODO новый заказ
    @FXML private void handleNewOrder(){
        mainApp.db.addOrder(currentOrder);
        product_ID.setText("");
        clientIDTB.setText("");
        amountTB.setText("");
        currentOrder = new Order();
        personTable.setItems(FXCollections.observableArrayList());

    }



    /////////////////////////////КЛИЕНТЫ////////////////////////////////
    @FXML private TextField clientNameTB;
    @FXML private TextField clientLastNameTB;
    @FXML private TextField patronymicTB;
    @FXML private TextField phoneNumberTB;
    @FXML private TextField birthdayTB;
    @FXML private void handleAddClient(){
        String name =clientNameTB.getText();
        String lastName = clientLastNameTB.getText();
        String  patronymic =patronymicTB.getText();
        String phoneNumber = phoneNumberTB.getText();
        LocalDate birthday = DateFormat.parse(birthdayTB.getText());
        if(name!="" && lastName!="" && patronymic!="" && phoneNumber!="" && birthday!=null){
            Client client = new Client(name, lastName, patronymic,phoneNumber,birthday);
            mainApp.db.addClient(client);
        }else{
            System.out.println("Неудача");
        }
    }
    ////////////////////////////////Заказ////////////////////////////////
    private Order currentOrder;

    @FXML private TextField amountTB;
    @FXML private TextField orderIDTB;
    @FXML private TextField clientIDTB;
    //TODO добавить позицию
    @FXML private void handleAddPosition(){
        Product product = mainApp.db.getProduct(Integer.parseInt(product_ID.getText()));
        currentOrder.addProduct(product,Integer.parseInt(amountTB.getText()));
        //personTable.getItems().removeAll()
        personTable.getItems().setAll(FXCollections.observableArrayList(currentOrder.productList));
    }
    @FXML private void handleSearchOrder(){

        currentOrder=mainApp.db.getOrder(Integer.parseInt(orderIDTB.getText()));
        personTable.getItems().setAll(FXCollections.observableArrayList(currentOrder.productList));
    }

    //////////////////////////////Критерии///////////////////////
    @FXML private TextField searchSaleTB;
    @FXML private TextField searchDateTB;
    @FXML private TableView<Client> searchTable;
    @FXML private  ComboBox<String> CB = new ComboBox<String>();
    @FXML private TableColumn<Client, String> column1;
    @FXML private TableColumn<Client, String> column2;
    @FXML private TableColumn<Client, String> column3;
    @FXML private TableColumn<Client, String> column4;
    @FXML private TableColumn<Client, String> column5;
    @FXML private void handleSearchButton(){
        ObservableList<Client> clients=null;
        if(CB.getSelectionModel().isSelected(0)) {
            clients = mainApp.db.getBirthdayClient(DateFormat.parse(searchDateTB.getText()),null);
        }
        if(CB.getSelectionModel().isSelected(1)){
            clients = mainApp.db.getBirthdayClient(DateFormat.parse(searchDateTB.getText()),Integer.parseInt(searchSaleTB.getText()));
        }
        if(CB.getSelectionModel().isSelected(2))
            clients = mainApp.db.getBirthdayClient(null,null);
        column1.setCellValueFactory(cellData -> (cellData.getValue().getName()));
        column2.setCellValueFactory(cellData -> cellData.getValue().getLastName());
        column4.setCellValueFactory(cellData -> cellData.getValue().getPhoneNumber());
        column5.setCellValueFactory(cellData -> cellData.getValue().getBirthday());
        column3.setCellValueFactory(cellData -> cellData.getValue().getPatronymic());
        searchTable.getItems().setAll(clients);
    }
}
