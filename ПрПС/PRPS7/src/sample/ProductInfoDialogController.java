package sample;

import Entity.Product;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import Entity.*;
public class ProductInfoDialogController {
    ProductPosition product;

    private Stage dialogStage;
    @FXML private Label productNameLabel;
    @FXML private Label productPriceLabel;
    @FXML private Label productArticleLabel;
    @FXML private Label productIDLabel;
    public ProductInfoDialogController(){};
    @FXML private void initialize() {

    }
    public void setProduct(ProductPosition p){
        product = p;
        System.out.print("assad"+ p.product.getArticle());
        showProductDetails();
    }
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
    private void showProductDetails() {
        if (product != null) {
            // Заполняем метки информацией из объекта person.
            productNameLabel.setText(product.product.getName());
            productPriceLabel.setText(Double.toString(product.product.getPrice()));
            productArticleLabel.setText(product.product.getArticle());
          //  productIDLabel.setText(Integer.toString(product.getID()));

            // TODO: Нам нужен способ для перевода дня рождения в тип String!
            // birthdayLabel.setText(...);
        } else {
            // Если product = null, то убираем весь текст.
            productNameLabel.setText("");
            productPriceLabel.setText("");
            productArticleLabel.setText("");
            productIDLabel.setText("");
        }
    }
}
