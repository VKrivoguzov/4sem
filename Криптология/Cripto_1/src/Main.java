//Реализовать генерацию большого простого числа (порядка ~ 2^100).
//Вариант 2: тест Соловея-Штрассена

import java.math.BigInteger;
import java.util.Random;

public class Main {
    public static boolean isProbablyPrime(BigInteger a,int k){
        Random rnd = new Random();
        for(int i=0;i<k;i++) {
            BigInteger b;
            do {
                b = new BigInteger(a.bitLength(), rnd);
            } while (a.compareTo(b) <= 0 || b.compareTo(BigInteger.ZERO)==0);
            if(a.gcd(b).compareTo(BigInteger.ONE)!=0) {
                return false;
            }
            if(b.modPow((a.subtract(BigInteger.ONE)).divide(BigInteger.TWO),a).compareTo(BigInteger.valueOf(getJakobiSymbol(b,a)).mod(a))!=0)
                return false;
        }
        return true;
    }
    static int getJakobiSymbol(BigInteger a, BigInteger b){
        if(a.gcd(b).compareTo(BigInteger.ONE)!=0)
            return 0;
        int r=1;
        while(a.compareTo(BigInteger.ZERO)!=0) {
            int t = 0;
            while (a.mod(BigInteger.TWO).compareTo(BigInteger.ZERO) == 0) {
                t++;
                a=a.divide(BigInteger.TWO);
            }
            if (t % 2 == 1)
                if ((b.mod(BigInteger.valueOf(8))).compareTo(BigInteger.valueOf(3))==0 || ((b.mod(BigInteger.valueOf(8))).compareTo(BigInteger.valueOf(5))==0))
                    r = -r;
            if (a.mod(BigInteger.valueOf(4)).compareTo(BigInteger.valueOf(3))==0 && b.mod(BigInteger.valueOf(4)).compareTo(BigInteger.valueOf(3))==0) {
                r = -r;
            }
            BigInteger c = a;
            a = b.mod(c);
            b = c;

        }
        return r;
    }

    //2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109
static int[] firstPrimes={2,5,7,11,13,17};
    public static boolean startTest(BigInteger a){
        for(int b: firstPrimes){
            if(a.mod(BigInteger.valueOf(b)).compareTo(BigInteger.ZERO)==0){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        int nBits = 3000;
        int k = 10; // количество раундов
        BigInteger a;
        int nIter=0;
        do {
            do {
                a = new BigInteger(nBits, new Random());
            } while (a.compareTo(BigInteger.ZERO) == 0 || a.compareTo(BigInteger.ONE) == 0 || a.compareTo(BigInteger.TWO.pow(1000))<=0);
            nIter++;
        } while (!startTest(a) || !isProbablyPrime(a, k));
        System.out.println(nIter);
        System.out.print(a.toString() + " простое с вероятностью "+ Double.toString(1-Math.pow(0.5,k))+"\n");
    }
}
