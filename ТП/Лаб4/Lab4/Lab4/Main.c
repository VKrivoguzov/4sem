#include <stdio.h>
#include <sqlite3.h>

int callback(void *NotUsed, int argc, char **argv, char **azColName) {
	NotUsed = 0;
	for (int i = 0; i < argc; i++) {
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	printf("\n");
	return 0;
}

void printMenu() {
	printf("[1] - ������� *\n");
	printf("[2] - ��������\n");
	printf("[3] - �������\n");
	printf("[4] - ������� by ...\n");
	printf("[5] - ���� ���������\n");
	printf("[0] - �����\n");
}



int insert(sqlite3* db) {
	printf("�������� �������:\n");
	printf("[1] - Employees\n");
	printf("[2] - Departament\n");
	char c = getchar();
	char sqlStatement[512];
	if (c == '1') {
		char tableInfo[9][64];
		printf("������� ID:");
		scanf("%s", tableInfo[0]);
		printf("������� ���:");
		scanf("%s", tableInfo[1]);
		printf("������� �������:");
		scanf("%s", tableInfo[2]);
		printf("������� ��������:");
		scanf("%s", tableInfo[3]);
		printf("������� ���� ��������:");
		scanf("%s", tableInfo[4]);
		printf("������� �����:");
		scanf("%s", tableInfo[6]);
		printf("������� ������:");
		scanf("%s", tableInfo[7]);
		printf("������� �����:");
		scanf("%s", tableInfo[8]);
		sprintf(sqlStatement, "INSERT INTO Employees (ID, Name, Surname, Patronymic, Birthday, Photo, City, Country, Address) VALUES (%s, \"%s\", \"%s\", \"%s\", \"%s\", \"?\", \"%s\", \"%s\", \"%s\");", tableInfo[0], tableInfo[1], tableInfo[2], tableInfo[3], tableInfo[4], tableInfo[6], tableInfo[7], tableInfo[8]);
		sqlite3_stmt *res;
		if (sqlite3_prepare_v2(db, sqlStatement, -1, &res, 0))
			fprintf(stderr, "�� �����: %s\n", sqlite3_errmsg(db));

		printf("������� ���� � ����:");
		scanf("%s", tableInfo[5]);
		FILE *fp = fopen("/Users/home/Documents/Misha/PrTech/labs/lab4/task5/user.jpg", "rb");
		fseek(fp, 0, SEEK_END);
		int nFileLen = (int)ftell(fp);
		fseek(fp, 0, SEEK_SET);
		char data[nFileLen + 1];
		fread(data, 1, nFileLen, fp);
		sqlite3_bind_blob(res, 1, data, nFileLen, NULL);
		sqlite3_step(res);
		sqlite3_finalize(res);
	}
	else if (c == '2') {
		char tableInfo[4][64];
		printf("������� ID:");
		scanf("%s", tableInfo[0]);
		printf("������� �����������:");
		scanf("%s", tableInfo[1]);
		printf("������� �������:");
		scanf("%s", tableInfo[2]);
		printf("������� ���� �������� �� ������:");
		scanf("%s", tableInfo[3]);
		sprintf(sqlStatement, "INSERT INTO Departament (ID, Department, Position, Day_of_empoyment) VALUES (%s, \"%s\", \"%s\", \"%s\");", tableInfo[0], tableInfo[1], tableInfo[2], tableInfo[3]);
		char *zErrMsg = 0;
		return sqlite3_exec(db, sqlStatement, callback, 0, &zErrMsg);
	}
	return -1;
}

int remove(sqlite3* db) {
	printf("Select a table:\n");
	printf("[1] - Employees\n");
	printf("[2] - Departament\n");
	char dec = getchar();
	int id;
	char sqlStatement[64];
	if (dec == '1') {
		printf("������� ID:");
		scanf("%d", &id);
		sprintf(sqlStatement, "DELETE from Employees where ID=%d;", id);
	}
	else if (dec == '2') {
		printf("������� ID:");
		scanf("%d", &id);
		sprintf(sqlStatement, "DELETE from Departament where ID=%d;", id);
	}
	else {
		return -1;
	}
	char *zErrMsg = 0;
	return sqlite3_exec(db, sqlStatement, callback, 0, &zErrMsg);
}

int selectBy(sqlite3* db) {
	printf("�������� ����:\n");
	printf("[1] - ID\n");
	printf("[2] - �������\n");
	printf("[3] - �����\n");
	char dec = getchar();
	if (dec == '1') {
		selectByID(db);
	}
	else if (dec == '2') {
		selectBySurname(db);
	}
	else if (dec == '3') {
		selectByCity(db);
	}
	else {
		return -1;
	}
	return 1;
}

int selectByID(sqlite3* db) {
	int id;
	printf("������� ID:");
	scanf("%d", &id);
	char sqlStatement[256];
	sprintf(sqlStatement, "SELECT * FROM Employees INNER JOIN Departament ON Employees.ID = Departament.ID WHERE Employees.ID = %d", id);
	char *zErrMsg = 0;
	return sqlite3_exec(db, sqlStatement, callback, 0, &zErrMsg);
}

int selectBySurname(sqlite3* db) {
	char surname[32];
	printf("������� �������:");
	scanf("%s", surname);
	char sqlStatement[256];
	sprintf(sqlStatement, "SELECT * FROM Employees INNER JOIN Departament ON Employees.ID = Departament.ID WHERE Employees.Surname = \"%s\"", surname);
	char *zErrMsg = 0;
	return sqlite3_exec(db, sqlStatement, callback, 0, &zErrMsg);
}

int selectByCity(sqlite3* db) {
	char city[32];
	printf("������� City:");
	scanf("%s", city);
	char sqlStatement[256];
	sprintf(sqlStatement, "SELECT * FROM Employees INNER JOIN Departament ON Employees.ID = Departament.ID WHERE Employees.City = \"%s\"", city);
	char *zErrMsg = 0;
	return sqlite3_exec(db, sqlStatement, callback, 0, &zErrMsg);
}

void getPhoto(sqlite3* db) {
	{
		sqlite3_stmt *res;
		if (sqlite3_prepare_v2(db, "SELECT ID, Photo FROM Employees where id=?", -1, &res, 0))
		{
			fprintf(stderr, "������: %s\n", sqlite3_errmsg(db));
			return;

		}
		else {
			printf("������� id ���������: ");
			int id;
			scanf("%d", &id);
			if (sqlite3_bind_int(res, 1, id)) {
				fprintf(stderr, "������: %s\n", sqlite3_errmsg(db));
				sqlite3_finalize(res);
				return;
			}
		}

		if (sqlite3_step(res) == SQLITE_ROW) {
			char filename[256] = "/Users/home/Documents/Misha/PrTech/labs/lab4/task5/userOut.jpg\0";
			char filename2[256];
			printf("������� ���� �����: ");
			scanf("%s", filename2);
			FILE* f = fopen(filename, "wb");
			fwrite(sqlite3_column_blob(res, 1), 1, sqlite3_column_bytes(res, 1), f);
			printf("���� ������� �: %s\n", filename);
			fclose(f);
		}
		else
			printf("�� �������!\n");
		sqlite3_finalize(res);
	}
}



int main(int argc, const char * argv[]) {
	sqlite3 *db;
	int rc;
	rc = sqlite3_open("..//task5.db", &db);
	
	char c;
	while ((c = getchar()) != '0') {		
		getchar(); 
		printMenu();
		switch (c) {
		case '1':
			rc = SELECTALL(db);
			break;
		case '2':
			rc = insert(db);
			getchar();
			break;
		case '3':
			rc = remove(db);
			getchar();
			break;
		case '4':
			rc = selectBy(db);
			getchar();
			break;
		case '5':
			getPhoto(db);
			getchar();
			break;
		default:
			break;
		}
	}
	sqlite3_close(db);
	return 0;
}

