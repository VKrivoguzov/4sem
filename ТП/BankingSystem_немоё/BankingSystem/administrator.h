

#ifndef administrator_h
#define administrator_h
#include <stdbool.h>
#include <stdio.h>

bool addClient();

bool updateClientEmail();

bool updateClientConnectInfo();

bool addAccount();

bool updateAccountCurrency();

bool updateAccountType();

#endif /* administrator_h */
