#include <iostream>
#include <random>
using namespace std;
double function(double x) {
	return 1.0 /(x*x*x*x + 3 * x*x*x + 17);
}

double Integral(int n) {
	random_device rd;  
	mt19937 gen(rd());
	uniform_real_distribution<> dis(-100,100);
	double integral=0;
	for (int i = 0; i<n; i++) {
		integral+=function(dis(gen))*(200.0);
	}
	return integral*2 / n;
}
int main() {

	cout << Integral(100)<<endl;
	cout << 1.0/(10*10*10*10) << endl;
	cout << Integral(100) << endl;
	int a;
	cin >> a;


	return 0;
}