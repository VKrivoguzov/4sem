import org.knowm.xchart.*;
import org.knowm.xchart.style.Styler;
import org.knowm.xchart.style.Theme;
import org.knowm.xchart.style.XYStyler;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Random;

public class Main {

    public static void main(String[] args)
    {
        System.out.println(firstIntegral(1000));
        System.out.println(secondIntegral(10000));
        int n=5,k=500;
        double[] xData = new double[n] ;
        double[] yData = new double[n] ;
        for(int j=0;j<k;j++)
        for(int i=1;i<=n;i++){
            yData[i-1]+=Math.abs(firstIntegral(Math.pow(10,i))-0.227)/k;
            xData[i-1]=(int)Math.pow(10,i);
        }
        CategoryChart chart1 = new CategoryChartBuilder().width(600).height(400).title("First integral").build();
        chart1.addSeries("First integral",xData,yData);
        chart1.getStyler().setDefaultSeriesRenderStyle(CategorySeries.CategorySeriesRenderStyle.Line);

        //==========================================
        for(int j=0;j<k;j++)
        for(int i=1;i<=5;i++){
            yData[i-1]+=Math.abs(secondIntegral(Math.pow(10,i))-11.7071)/k;
            xData[i-1]=(int)Math.pow(10,i);
        }
        CategoryChart chart2 = new CategoryChartBuilder().width(600).height(400).title("Second integral").build();
        chart2.addSeries("Second integral",xData,yData);
        chart2.getStyler().setDefaultSeriesRenderStyle(CategorySeries.CategorySeriesRenderStyle.Line);

        new SwingWrapper(chart2).displayChart();
        new SwingWrapper(chart1).displayChart();
    }

    public static double second(double x,double y){
        return (x*y*y+1)*Math.sin(x);
    }
   static MathContext mc = new MathContext(20, RoundingMode.CEILING);
    public static double secondIntegral(double n){
        double integral = 0;
        Random rnd = new Random();
        for(int i=0;i<n;i++){
            double y=rnd.nextDouble()*6-3;
            double x=rnd.nextDouble()*2*(3-Math.abs(y))-(3-Math.abs(y));
            integral+=second(x,y)*12*(3-Math.abs(y));
        }
        return integral/n;
    }

    static double P(double x,double m,double d){
        return (Math.exp(-(x-m)*(x-m)/2/d/d))/(d*Math.sqrt(2*Math.PI));
    }
    public static double firstFunction(double x){
        return 1/(x*x*x*x+3*x*x+17);
    }
    public static double firstIntegral(double n){
        double integral = 0;
        Random rnd = new Random();
        for(int i=0;i<n;i++){
            double r=rnd.nextDouble()*20-10;
            integral+=firstFunction(r)*20;///P(r,0,1);
        }
        return integral/n;
    }
}
