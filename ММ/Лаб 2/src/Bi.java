import java.util.Random;

//Биномиальное – Bi(m,p), m = 6, p = 0.3333333;
public class Bi {

    int m;
    double p;
    Random r;
    public Bi(){
        m=6;
        p=0.3333333;
        r = new Random();
    }

    public Bi(int m, double p){
        if( p<0 || p>1 || m<=0)
            throw new IllegalArgumentException();
        this.m=m;
        this.p=p;
        r = new Random();
    }

    public int getNext(){
        int x=0;
        for (int i=0;i<m;i++){
            double a=r.nextDouble() ;
            if(a<p)
            x++;
        }
        return x;
    }
    public double getDispersion(){
        return m*p*(1-p);
    }
    public double getExpectation(){
        return m*p;
    }

    protected  int bci(int n,int k)
    {
        if (k>n/2) k=n-k;
        if (k==1)  return n;
        if (k==0)  return 1;
        int r=1;
        for (int i=1; i<=k;++i) {
            r*=n-k+i;
            r/=i;
        }
        return r;
    }
    public double getTeoreticalP(int k){
        return bci(m,k)*Math.pow(p,k)*Math.pow(1-p,m-k);
    }
}
