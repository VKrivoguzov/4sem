import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

//Вариант 5     Биномиальное – Bi(m,p), m = 6, p = 0.3333333;
// Отрицательное биномиальное –  (r,p), r = 4, p = 0.2;
public class Main {
    static double tableValues[] = {0, 3.8, 6.0, 7.8, 9.5, 11.1, 12.6,14.1,15.5,16.9,18.3,19.7,21,22.4,23.7,25,26.3,27.6,28.9,30.1,31.4,32.7,33.9,35.2,36.4,37.7,38.9,40.1,41.3,42.6,43.8};

    public static void main(String[] args) {
        //Bi nb=new Bi();

        System.out.println("-----------------------------Биномиальное----------------------------------");
        taskBi(new Bi(), 1000);
        System.out.println("------------------------Негативное биномиальное----------------------------");
        taskNegBi(new NegBi(),1000);
        System.out.println("---------------------------------------------------------------------------");
    }

    static int[] getBiArr(Bi b, int n) {
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = b.getNext();
        }
        return arr;
    }

    static double getExpectation(int[] arr) {
        double x = 0;
        for (int a : arr) {
            x += a;
        }
        x /= arr.length;
        return x;
    }

    static double getDispersion(int[] arr) {
        double x = 0;
        double E = getExpectation(arr);
        for (int a : arr) {
            x += Math.pow((a - E), 2);
        }
        x /= (arr.length - 1);
        return x;
    }
    //===============================Биномиальное===============================
    static void taskBi(Bi b, int n) {

        int[] arr = getBiArr(b, n);
        double E = getExpectation(arr);
        System.out.println("Несмещенная оценка матожидания = " + E);
        System.out.println("Теоритическое матожидание = " + b.getExpectation());///r*q/p
        double D = getDispersion(arr);
        System.out.println("Несмещенная оценка дисперсии = " + D);
        System.out.println("Теоритическое дисперсия = " + b.getDispersion());///r*q/p/p
        System.out.println("Тест Пирсона пройден?"+PirsonTestBi(arr,b));
        System.out.format("%.4f\n",errorTestBi(n,2000));

    }
    static double errorTestBi(int n, int nTest) {
        int falseNumber = 0;
        for (int i = 0; i < nTest; i++) {
            Bi b = new Bi();
            int[] arr = getBiArr(b, n);
            if (!PirsonTestBi(arr, b))
                falseNumber++;
        }
        return (double) falseNumber / nTest;
    }

    static boolean PirsonTestBi(int arr[], Bi b) {
        int m[]= new int[b.m + 1];
        Arrays.fill(m, 0);
        for (int i = 0; i < arr.length; i++) {
            m[arr[i]]++;
        }
        double sumXX = 0;
        for (int i = 0; i < m.length; i++) {
            double p = b.getTeoreticalP(i);
            sumXX += Math.pow(m[i] - p * arr.length, 2.0) / (p * arr.length);
        }
        return sumXX <= tableValues[m.length-1];
    }

    //===============================Негативное биномиальное===============================
    static void taskNegBi(Bi b, int n) {
        int[] arr = getBiArr(b, n);
        double E = getExpectation(arr);
        System.out.println("Несмещенная оценка матожидания = " + E);
        System.out.println("Теоритическое матожидание = " + b.getExpectation());///r*q/p
        double D = getDispersion(arr);
        System.out.println("Несмещенная оценка дисперсии = " + D);
        System.out.println("Теоритическое дисперсия = " + b.getDispersion());///r*q/p/p
        System.out.println("Тест Пирсона пройден?"+PirsonTestNegBi(arr,b));
        System.out.format("%.4f\n",errorTestNegBi(n,1000));
        //for(   int i=0;i<10;i++)
            //System.out.print(b.getTeoreticalP(i)+" ");

    }
    static int getDifferentNumber(int arr[]){
        Set<Integer> set = new HashSet<>();
        for(int i: arr){
            set.add(i);
        }
        return set.size();
    }
    static boolean PirsonTestNegBi(int arr[], Bi b){
        int nInterval=getDifferentNumber(arr);
        if(nInterval>30)
            nInterval=30;
        double max=0;
        Arrays.sort(arr);
        for (int i:arr) {
            max=Math.max(i,max);
        }
        max+=0.5;
        double delta = (double)max/(nInterval);
        int[] m= new int[nInterval];
        for (int i = 0; i < arr.length; i++) {
            m[(int)(arr[i]/delta)]++;
        }
        double sumXX = 0;
        for (int i = 0; i < m.length-1; i++) {
            double p = 0;
          //  System.out.format("%d) [%d,%d] ",i, (int)Math.ceil(i * delta),(int)((i + 1) * delta));
            for (int j = (int)Math.ceil(i * delta); j < ((i + 1) * delta); j++) {
                p += b.getTeoreticalP(j);
            }
            p *= arr.length;
            sumXX += Math.pow((double) (m[i] - p), 2.0) / p ;
          //  System.out.println(p+" "+(double)m[i]+" "+sumXX);
        }
        return sumXX <= tableValues[nInterval-1];
    }
    static double errorTestNegBi(int n, int nTest) {
        int falseNumber = 0;
        for (int i = 0; i < nTest; i++) {
            Bi b = new NegBi();
            int[] arr = getBiArr(b, n);
            if (!PirsonTestNegBi(arr, b))
                falseNumber++;
        }
        return (double) falseNumber / nTest;
    }




}
