import java.util.Random;

public class NegBi extends Bi{


    public NegBi(){
        m=4;
        p=0.2;
        r=new Random();
    }
    public NegBi(int m, double p){
        super(m,p);
    }

    public  double getTeoreticalP(int k){
        return bci(k+m-1,k)*Math.pow(p,m)*Math.pow(1-p,k);
    }

    public double getDispersion(){
        return m*(1-p)/(p*p);
    }
    public double getExpectation(){
        return m*(1-p)/p;
    }
    @Override
    public int getNext(){
        int fails=0;
        int succesCount=0;
        while(succesCount!=m){
            double a= r.nextDouble();
            if(a<p)
                succesCount++;
            else
                fails++;
        }
        return fails;
    }
}
