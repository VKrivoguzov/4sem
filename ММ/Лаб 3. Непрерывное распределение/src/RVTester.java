import java.util.Arrays;
import java.util.Set;

public class RVTester {
    static double tableValues[] = {0, 3.8, 6.0, 7.8, 9.5, 11.1, 12.6,14.1,15.5,16.9,18.3,19.7,21,22.4,23.7,25,26.3,27.6,28.9,30.1,31.4,32.7,33.9,35.2,36.4,37.7,38.9,40.1,41.3,42.6,43.8};

    RandomVariable rv;


    public RVTester(RandomVariable rv){
        this.rv=rv;
    }
    public double[] getArr(int n){
        double[] arr= new double[n];
        for (int i=0;i<n;i++){
            arr[i]=rv.getNext();
        }
        return arr;
    }
    public double getEmpiricalExpectation(double[] arr){
        double x = 0;
        for (double a : arr) {
            x += a;
        }
        x /= arr.length;
        return x;
    }
    public void ExpectationTest(double[] arr){
        System.out.println("Несмещенная оценка матожидания = " + getEmpiricalExpectation(arr));
        System.out.println("Теоритическое матожидание = " + rv.getExpectation());
    }
    public double getEmpiricalDispersion(double[] arr){
        double x = 0;
        double E = getEmpiricalExpectation(arr);
        for (double a : arr) {
            x += Math.pow((a - E), 2);
        }
        x /= (arr.length - 1);
        return x;
    }
    public void DispersionTest(double[] arr){
        System.out.println("Несмещенная оценка дисперсии = " + getEmpiricalDispersion(arr));
        System.out.println("Теоритическая дисперсия = " + rv.getDispersion());
    }
    //==================================Kolmogor Test =============================================
    public double empiricalF(double x, double arr[]) {
        int n=0;
        for (int i=0; i<arr.length; i++)
            if(x>=arr[i])
                n++;
        return (double) (n) / arr.length;
    }
    boolean KolmogorovTest(double arr[]) {
        double d = 0;
        for (int i = 0; i < arr.length; i++) {
            d = Math.max(d, Math.abs((double)empiricalF(arr[i], arr) - rv.getF(arr[i])));
        }
        return Math.sqrt(arr.length) * d < 1.36;
    }
    public double errorKolmogorovTest(int n, int nTest) {
        int falseNumber = 0;
        for (int i = 0; i < nTest; i++) {
            double[] arr = getArr(n);
            if (!KolmogorovTest(arr))
                falseNumber++;
        }
        return (double) falseNumber / nTest;
    }
    //====================================Pirson test===============================================
    public boolean PirsonTest(double arr[]){
        int nInterval=30;
        double max=Double.MIN_VALUE;
        double min = Double.MAX_VALUE;
        for (double i:arr) {
            max=Math.max(i,max);
            min=Math.min(i,min);
        }
        double delta = (double)(max-min)/(nInterval);
        int[] m= new int[nInterval];
        for (int i = 0; i < arr.length; i++) {
            if(arr[i]==max)
                m[m.length-1]++;
            else
                m[(int)((arr[i]-min)/delta)]++;
        }
        double sumXX = 0;
        double addP=0;
        for (int i = 0; i < m.length; i++) {
            double   p = rv.getF((i + 1) * delta+min)- rv.getF(i * delta+min);
            p *= arr.length;
            if(m[i]<10){
                nInterval--;
                if(i+1<m.length)
                    m[i+1]+=m[i];
                addP+=p;
                continue;
            }
            p+=addP;
            sumXX += Math.pow((double) (m[i] - p), 2.0) / p;
            addP=0;
        }
        return sumXX <= tableValues[nInterval-1];
    }
    public double errorPirsonTest(int n,int nTest){
        int falseNumber = 0;
        for (int i = 0; i < nTest; i++) {
            double[] arr = getArr(n);
            if (!PirsonTest(arr))
                falseNumber++;
        }
        return (double) falseNumber / nTest;
    }

    //====================================Error test================================================
    public void printErrorTest(int n,int nTest){
        System.out.format("Критерий Колмогорова. Вероятность ошибки I рода = %.2f\n",errorKolmogorovTest(n,nTest));
        System.out.format("Критерий Пирсона. Вероятность ошибки I рода = %.2f\n",errorPirsonTest(n,nTest));
    }



}
