import java.util.Random;

public class ExponentialRV implements RandomVariable{
    double a;
    Random rnd;
    public ExponentialRV(){
        this.a=0.5;
        rnd=new Random();
    }
    public ExponentialRV(double a){
        this.a=a;
        rnd=new Random();
    }

    public double getNext(){
        return -Math.log(1-rnd.nextDouble())/a;
    }
    //Теоретическое мат ожидание
    public double getExpectation(){
        return 1/a;
    }
    //Теоретическая дисперсия
    public double getDispersion(){
        return 1/a/a;
    }

    public double getF(double x){
        return 1-Math.exp(-a*x);
    }
}
