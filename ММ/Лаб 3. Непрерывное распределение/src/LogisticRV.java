import java.util.Random;

public class LogisticRV implements RandomVariable{
    double a;
    double b;
    Random rnd;
    public LogisticRV(){
        this.a=0;
        this.b=1.5;
        rnd=new Random();
    }
    public LogisticRV(double a,double b){
        this.a=a;
        this.b=b;
        rnd=new Random();
    }

    public double getNext(){
        return a-b*Math.log(1/rnd.nextDouble()-1);
    }
    //Теоретическое мат ожидание
    public double getExpectation(){
        return a;
    }
    //Теоретическая дисперсия
    public double getDispersion(){
        return Math.pow(Math.PI*b,2)/3;
    }

    public double getF(double x){
        return 1/(1+Math.exp(-(x-a)/b));
    }
}
