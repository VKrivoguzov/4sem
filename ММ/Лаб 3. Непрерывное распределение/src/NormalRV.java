import java.util.Random;
import org.apache.commons.math3.special.Erf;

public class NormalRV implements RandomVariable{
    double m;
    double s;
    Random rnd;
    int n;

    public NormalRV(){
        this.m=-4;
        this.s=4;
        n=12;
        rnd=new Random();
    }

    public NormalRV(double m,double s,int n){
        this.m=m;
        this.s=s;
        this.n=n;
        rnd=new Random();
    }

    public double getNext(){
        double sum=0;
        for(int i=0;i<n;i++){
            sum+=rnd.nextDouble();
        }
        return m+(Math.sqrt(s))*Math.sqrt(12/n)*(sum-n/2);
    }
    //Теоретическое мат ожидание
    public double getExpectation(){
        return m;
    }
    //Теоретическая дисперсия
    public double getDispersion(){
        return s;
    }

    public double getF(double x){
        return (1+Erf.erf((x-m)/Math.sqrt(2*s)))/2;
    }
}

