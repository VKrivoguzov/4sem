public interface RandomVariable {
    double getNext();
    double getExpectation();
    double getDispersion();
    double getF(double x);
}
