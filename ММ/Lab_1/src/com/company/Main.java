package com.company;

import Sensors.BSV;
import Sensors.MMBSV;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        int a0, b;
        a0 = b = 68921;
        int K = 48;

        {
            BSV bsv1 = new BSV(a0, b);
            BSV bsv2= new BSV(68923,68923);
            MMBSV bsv3= new MMBSV(K,bsv1,bsv2);
            int n = 1000;
            double bsvArr[] = new double[n];

            for (int i = 0; i < n; i++) {
                bsvArr[i] = bsv1.getNext();
            }

            System.out.println("=======================================================================");
            System.out.println(Arrays.toString(bsvArr));
            System.out.print("Мультипликативно конгруентный датчик ");
            System.out.println("Тест Колмогорова пройден успешно?"+ KolmogorovTest(bsvArr));
            System.out.println("Тест Пирсона пройден успешно?"+PirsonTest(bsvArr));
            for (int i = 0; i < n; i++) {
                bsvArr[i] = bsv3.getNext();
            }
            System.out.println("=======================================================================");
            System.out.println(Arrays.toString(bsvArr));
            System.out.print("Датчик Макларена-Марсальи");
            System.out.println("Тест Колмогорова пройден успешно?"+ KolmogorovTest(bsvArr));
            System.out.println("Тест Пирсона пройден успешно?"+PirsonTest(bsvArr));
            System.out.println("=======================================================================");
        }

    }
//==================================Kolmogor Test =============================================
static double empiricalF(double x, double arr[]) {
    Arrays.sort(arr);
    if (x < arr[0])
        return 0;
    if (x >= arr[arr.length - 1])
        return 1;
    int i = 0;
    for (; arr[i] <= x; i++) ;
    return (double) (i + 1) / arr.length;
}
    static boolean KolmogorovTest(double arr[]) {
        double d = 0;
        for (int i = 0; i < arr.length; i++) {
            d = Math.max(d, Math.abs((double)empiricalF(arr[i], arr) - arr[i]));
        }
        if(Math.sqrt(arr.length) * d < 1.36)
            return true;
        return false;
    }
    //==================================================Pirson Test==============================

    static boolean PirsonTest(double arr[]) {
        ArrayList<Double> x = new ArrayList<Double>();
        for(double d:arr) {
           if(!x.contains(d))
               x.add(d);
        }
        int m[]=new int[x.size()];
        Arrays.fill(m,0);
        for(double d:arr) {
            m[x.indexOf(d)]++;
        }
        double sumXX=0;
        for(int d: m){
            sumXX+=Math.pow(((double)d/arr.length-1.0/m.length),2.0)*m.length;
        }
        return (sumXX<0.05);
    }


}

