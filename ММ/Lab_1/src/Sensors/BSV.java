package Sensors;

public class BSV {
    private int b;
    private long M;
    private int A;

    public BSV(int a0, int b) {
        if (a0 <= 0 || b <= 0)
            throw new IllegalArgumentException("Arguments are not natural");
        this.b = b;
        M = Integer.MAX_VALUE + 1L;
        A = a0;
    }

    public double getNext() {
        A = b * A;
        if (A < 0)
            A += Integer.MAX_VALUE + 1;
        return (double)A / M;
    }
}
