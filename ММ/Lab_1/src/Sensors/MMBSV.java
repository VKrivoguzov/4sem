package Sensors;

public class MMBSV {
    private double[] V;
    BSV b, c;

    public MMBSV(int k, BSV b, BSV c) {
        V = new double[k];
        this.b = b;
        this.c = c;
        for (int i=0;i<V.length;i++) {
            V[i] = b.getNext();
        }
    }


    public double getNext() {
        int index = (int) (c.getNext() * V.length);
        double a = V[index];
        V[index]=b.getNext();
        return a;
    }

}
