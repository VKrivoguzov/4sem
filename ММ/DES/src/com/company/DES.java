package com.company;
import java.util.Random;

public class DES {
    private long key;
    private long keys[];
    public DES(){
        key=new Random().nextLong();
        keys=generateKey(key);
    }
    public DES(long key){
        setKey(key);
    }
    public DES(String key){
        setKey(key);
    }
    public void setKey(long key){
        this.key=key;
        keys=generateKey(this.key);
    }
    public void setKey(String key){
        this.key=fromStringToLong(key);
        keys=generateKey(this.key);
    }
    //========================Permutation========================================
    private boolean isBit(long a, int n) {
        return (a & (1L << (n - 1))) != 0;
    }
    private long setBit(long a, int n, boolean value) {
        long bit = 1L << (n - 1);
        if (value)
            a |= bit;
        else
            a &= ~bit;
        return a;
    }
    private long permutation(long block, int n, int[] P) {
        long result = 0;
        for (int i = 0; i < P.length; i++) {
            result = setBit(result, P.length - i, isBit(block, n - P[i] + 1));
        }
        return result;
    }
    //===========================Keys generating==================================
    private final static int[] shift = {1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1};
    private int shift(int key, int i) {
        int result = (key << shift[i]) | (key >> (28 - shift[i]));
        result &= ~(11 << 28);
        return result;
    }
    private final static int[] keyPermutation = {
            57, 49, 41, 33, 25, 17, 9, 1,
            58, 50, 42, 34, 26, 18, 10, 2,
            59, 51, 43, 35, 27, 19, 11, 3,
            60, 52, 44, 36, 63, 55, 47, 39,
            31, 23, 15, 7, 62, 54, 46, 38,
            30, 22, 14, 6, 61, 53, 45, 37,
            29, 21, 13, 5, 28, 20, 12, 4};
    private final static int[] compressPermutation = {
            14, 17, 11, 24, 1, 5, 3, 28,
            15, 6, 21, 10, 23, 19, 12, 4,
            26, 8, 16, 7, 27, 20, 13, 2,
            41, 52, 31, 37, 47, 55, 30, 40,
            51, 45, 33, 48, 44, 49, 39, 56,
            34, 53, 46, 42, 50, 36, 29, 32};
    private long[] generateKey(long key) {
        long clearKey = permutation(key, 64, keyPermutation);
        int C = (int) clearKey;
        int D = (int) (clearKey >> 28);
        C &= 0x0FFFFFFF;
        D &= 0x0FFFFFFF;
        long roundKeys[] = new long[16];
        for (int i = 0; i < shift.length; i++) {
            D = shift(D, i);
            C = shift(C, i);
            long temp = ((long) D << 28L) |  (C&0xFFFFFFFl);
            roundKeys[i] = permutation(temp, 56, compressPermutation);
        }
        return roundKeys;
    }
    //==============================Func===============================================
    private final static int[] extendPermutation = {
            32, 1, 2, 3, 4, 5,
            4, 5, 6, 7, 8, 9,
            8, 9, 10, 11, 12, 13,
            12, 13, 14, 15, 16, 17,
            16, 17, 18, 19, 20, 21,
            20, 21, 22, 23, 24, 25,
            24, 25, 26, 27, 28, 29,
            28, 29, 30, 31, 32, 1};
    private static final int[][] S = {
            {
                    14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7,
                    0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8,
                    4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0,
                    15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13
            },
            {
                    15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10,
                    3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5,
                    0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15,
                    13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9
            },
            {
                    10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8,
                    13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1,
                    13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7,
                    1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12
            },
            {
                    7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15,
                    13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9,
                    10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4,
                    3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14
            },
            {
                    2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9,
                    14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6,
                    4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14,
                    11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3
            },
            {
                    12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11,
                    10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8,
                    9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6,
                    4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13
            },
            {
                    4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1,
                    13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6,
                    1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2,
                    6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12

            },
            {
                    13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7,
                    1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2,
                    7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8,
                    2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11
            }
    };
    private final static int[] ffPermutation =
            {
                    16, 7, 20, 21, 29, 12, 28, 17,
                    1, 15, 23, 26, 5, 18, 31, 10,
                    2, 8, 24, 14, 32, 27, 3, 9,
                    19, 13, 30, 6, 22, 11, 4, 25
            };
    private int f(int R, long key) {
        long extendR = permutation(R, 32, extendPermutation);
        extendR ^= key;
        int result = 0;
        for (int k = 7; k >= 0; k--) {
            long i = (((extendR & 32) >> 4) | (extendR & 1)) & (3);
            long j = (extendR >> 1) & 15;
            long l = S[k][(int) (i * 16 + j)];
            result |= l << (4 * (7 - k));
            extendR >>= 6;
        }
        return (int) permutation(result, 32, ffPermutation);
    }
    //==================================Encryption/Decryption=================================
    static int initialPermutation[] =
            {
                    58, 50, 42, 34, 26, 18, 10, 2, 60, 52, 44, 36, 28, 20, 12, 4,
                    62, 54, 46, 38, 30, 22, 14, 6, 64, 56, 48, 40, 32, 24, 16, 8,
                    57, 49, 41, 33, 25, 17, 9, 1, 59, 51, 43, 35, 27, 19, 11, 3,
                    61, 53, 45, 37, 29, 21, 13, 5, 63, 55, 47, 39, 31, 23, 15, 7
            };

    static int finalPermutation[] =
            {
                    40, 8, 48, 16, 56, 24, 64, 32, 39, 7, 47, 15, 55, 23, 63, 31,
                    38, 6, 46, 14, 54, 22, 62, 30, 37, 5, 45, 13, 53, 21, 61, 29,
                    36, 4, 44, 12, 52, 20, 60, 28, 35, 3, 43, 11, 51, 19, 59, 27,
                    34, 2, 42, 10, 50, 18, 58, 26, 33, 1, 41, 9, 49, 17, 57, 25
            };
    private long encrypt(long input) {
        input = permutation(input, 64, initialPermutation);
        int left = (int) (input >> 32);
        int right = (int) input;
        for (int i = 0; i < 16; i++) {
            int temp = right;
            right = left ^ f(right, keys[i]);
            left = temp;
        }
        input = ((long) right << 32) | left & 0xFFFFFFFFl;
        input = permutation(input, 64, finalPermutation);
        return input;
    }
    private long decrypt(long input) {
        input = permutation(input, 64, initialPermutation);
        int right = (int) (input >> 32);
        int left = (int) input;
        for (int i = 15; i >= 0; i--) {
            int temp = left;
            left = right ^ f(left, keys[i]);
            right = temp;
        }
        long l = (long) (right);
        input = (((long) left << 32) | right & 0xffffffffl);
        input = permutation(input, 64, finalPermutation);
        return input;
    }
    //========================================String decryption/encryption======================
    private long fromStringToLong(String str){
        if(str.length()!=4)
            throw new IllegalArgumentException("Key's length must be equal 4");
        int ll, lr, rl, rr;
        ll = str.charAt(0);
        lr = str.charAt(1);
        rl = str.charAt(2);
        rr = str.charAt(3);
        return  ((long) ((ll << 16) | lr) << 32) | (((rl << 16) | rr) & 0xFFFFFFFFl);
    }
    public String encrypt(String input) {
        int i;
        while (input.length() % 4 != 0)
            input = input + (char) (0);
        StringBuilder c = new StringBuilder();
        for (i = 0; i < input.length(); i += 4) {
            long l=fromStringToLong(input.substring(i,i+4));
            l = encrypt(l);
            c.append((char) (l >> 48));
            c.append((char) (l >> 32));
            c.append((char) (l >> 16));
            c.append((char) l);
        }
        return c.toString();
    }

    public String decrypt(String input) {
        int i;
        StringBuilder c = new StringBuilder();
        for (i = 0; i < input.length(); i += 4) {
            int ll, lr, rl, rr;
            ll = input.charAt(i);
            lr = input.charAt(i + 1);
            rl = input.charAt(i + 2);
            rr = input.charAt(i + 3);
            long l = ((((long) ll << 16) | lr) << 32) | (((rl << 16) | rr)&0xFFFFFFFFl);
            l = decrypt(l);
            c.append((char) (l >> 48));
            c.append((char) (l >> 32));
            c.append((char) (l >> 16));
            c.append((char) l);
        }
        return c.toString();
    }


}
